// ==UserScript== 
// @name       Mouse Control
// @namespace  http://*.koalabeast.com:*
// @version    1.0
// @description  Lets you use the mouse to steer your ship. Hold down the mouse button, and move the cursor in the direction you wish to go (simulates up/down/left/right based on the cursor position)
// @match      http://koalabeast.com
// @copyright  2014+, Patabugen ( www.patabugen.co.uk )
// @include    http://*.koalabeast.com:*
// ==/UserScript==

tagpro.ready(function(){
    
    // Grab the stuff we need to bind to. There may be a better way to get the canvas element?
    var canvas = document.getElementById('viewPort');
    var context = tagpro.api.backgroundContext;
    
    // We only act if the mouse is down
    var mouseIsDown = false;
    
    // Create all the events so we can trigger them as required
    var moveLeft = jQuery.Event("keydown", { keyCode: tagpro.keys.left[0] });
    var moveRight = jQuery.Event("keydown", { keyCode: tagpro.keys.right[0] });
    var moveUp = jQuery.Event("keydown", { keyCode: tagpro.keys.up[0] });
    var moveDown = jQuery.Event("keydown", { keyCode: tagpro.keys.down[0] });
    
    // The opposite events, so we can stop them
    var stopLeft = jQuery.Event("keyup", { keyCode: tagpro.keys.left[0] });
    var stopRight = jQuery.Event("keyup", { keyCode: tagpro.keys.right[0] });
    var stopUp = jQuery.Event("keyup", { keyCode: tagpro.keys.up[0] });
    var stopDown = jQuery.Event("keyup", { keyCode: tagpro.keys.down[0] });
    
    // Keep track of the state of the mouse    
    canvas.addEventListener('mousedown', function(evt) {
        mouseIsDown = true
    });
    canvas.addEventListener('mouseup', function(evt) {
        mouseIsDown = false
        // If the mouse comes up, stop doing anything
        jQuery(document).trigger( stopUp );
        jQuery(document).trigger( stopDown );
        jQuery(document).trigger( stopLeft );
        jQuery(document).trigger( stopRight );
    });
    
    // Whenver the mouse moves, update the keys we're simulating
    canvas.addEventListener('mousemove', function(evt) {
        if(!mouseIsDown) {
            return;
        }
        var rect = canvas.getBoundingClientRect();
        mousePos = {
            x: evt.clientX - rect.left,
            y: evt.clientY - rect.top
        };
        // Left/Right
        console.log(rect.width/2);
        
        if (mousePos.x < rect.width / 2) {
            // Left half of the canvas
            jQuery(document).trigger( moveLeft );
            jQuery(document).trigger( stopRight );
        } else {
            // Right half of the canvas
            jQuery(document).trigger( moveRight );
            jQuery(document).trigger( stopLeft );
        }
        if (mousePos.y < rect.height / 2) {
            // Top half of the canvas
            jQuery(document).trigger( moveUp );
            jQuery(document).trigger( stopDown );
        } else {
            // Bottom half of the canvas
            jQuery(document).trigger( moveDown );
            jQuery(document).trigger( stopUp );
        }
    }, false)
});
